from sqlalchemy import ForeignKey, Integer, Column, String, TIMESTAMP, Text
from sqlalchemy.orm import relationship

from .database import Base


class AbstractUser(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)


class BaseModel(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    slug = Column(String, unique=True)


class Client(AbstractUser):
    __tablename__ = 'ckillers_client'

    token = Column(String)


class Project(BaseModel):
    __tablename__ = 'ckillers_project'

    client_id = Column(Integer, ForeignKey('ckillers_client.id'))
    client = relationship(Client)


class Pipeline(BaseModel):
    __tablename__ = 'ckillers_pipeline'

    type = Column(String)
    project_id = Column(Integer, ForeignKey('ckillers_project.id'))
    project = relationship('Project')
    stage = relationship('Stage')


class Stage(BaseModel):
    __tablename__ = 'ckillers_stage'

    uri = Column(String)
    pipeline_id = Column(Integer, ForeignKey('ckillers_pipeline.id'))
    pipeline = relationship("Pipeline")
    event = relationship('Event')


class Event(Base):
    __tablename__ = 'ckillers_event'

    id = Column(Integer, primary_key=True, index=True)
    stage_id = Column(Integer, ForeignKey('ckillers_stage.id'))
    job_id = Column(String)
    stage = relationship("Stage")
    start = Column(TIMESTAMP)
    end = Column(TIMESTAMP)

class PipelineMessage(Base):
    __tablename__ = 'ckillers_pipelinemessage'

    id = Column(Integer, primary_key=True, index=True)
    timestamp = Column(TIMESTAMP)
    message = Column(String)
    pipeline_id = Column(Integer, ForeignKey('ckillers_pipeline.id'))
    job_id = Column(String)
    metadata_str = Column('metadata', Text)
    status = Column(String)
