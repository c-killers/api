from starlette.responses import JSONResponse
from typing import Optional, List
from pydantic import BaseModel

from . import models


class GenericMessageRequest(BaseModel):
    id: Optional[str] = None
    buildName: Optional[str] = None
    buildId: Optional[str] = None
    buildNumber: Optional[str] = None
    job: Optional[str] = None
    jobId: Optional[str] = None
    jobNumber: Optional[str] = None

    status: Optional[str] = None
    result: Optional[str] = None
    currentResult: Optional[str] = None
    event: Optional[str] = None

    message: Optional[str] = None
    detail: Optional[str] = None
    description: Optional[str] = None
    text: Optional[str] = None


def get_status(db):
    try:
        db.query(models.Project).all()
        db_status = 'ok'
    except Exception as e:
        print(e)
        db_status = 'error'
    finally:
        return JSONResponse(content={'apiServer':'ok', 'DB': db_status})
