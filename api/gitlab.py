from typing import Optional, List
from pydantic import BaseModel, datetime_parse, validator
from datetime import datetime

class KV(BaseModel):
    key: str
    value: str

class Author(BaseModel):
    name: str
    email: str

class User(BaseModel):
    name: str

class Runner(BaseModel):
    id: int
    description: str
    active: bool
    is_shared: bool

class ArtifactsFile (BaseModel):
    filename: str = None
    size: str = None

class Commit(BaseModel):
    id: str
    message: str
    timestamp: datetime
    url: Optional[str] = None
    author: Optional[Author] = None

class CommitEvent(BaseModel):
    id: int
    status: str

class Repository(BaseModel):
    name: str
    url: str


class ObjectAtributes(BaseModel):
    id: int
    ref: str
    tag: bool
    sha: str
    before_sha: str
    source: str
    status: str
    detailed_status: str = None
    stages: List[str]
    created_at: datetime
    finished_at: datetime = None
    duration: int = None
    variables: List[KV]

    @validator('created_at', 'finished_at',  pre=True)
    def parse_date(cls, v):
        if isinstance(v, str):
            return datetime.strptime(v,'%Y-%m-%d %H:%M:%S %Z')
        return v


class MergeRequest(BaseModel):
    id: int
    iid: int
    title: str
    source_branch: str
    source_project_id: int
    target_branch: str
    target_project_id: str
    state: str
    merge_status: str
    url: str


class Project(BaseModel):
    id: int
    name: str
    description: str
    web_url: str
    avatar_url: str = None
    git_ssh_url: str
    git_http_url: str
    namespace: str
    visibility_level: int
    path_with_namespace: str
    default_branch: str

class Build(BaseModel):
    id: int
    stage: str
    name: str
    status: str
    created_at: datetime
    started_at: datetime = None
    finished_at: datetime = None
    when: str
    manual: bool
    allow_failure: Optional[bool] = None
    user: User
    runner: Runner = None
    artifacts_file: ArtifactsFile

    @validator('created_at', 'started_at', 'finished_at',  pre=True)
    def parse_date(cls, v):
        if isinstance(v, str):
            return datetime.strptime(v,'%Y-%m-%d %H:%M:%S %Z')
        return v

class GitlabHookPipelineEvent(BaseModel):
    object_kind: str
    object_attributes: ObjectAtributes
    merge_request: MergeRequest = None
    user: User
    project: Project
    commit: Commit
    builds: List [Build]

class GitlabHookBuildEvent(BaseModel):
    object_kind: str
    build_id: str
    build_name: str
    build_status: str
    build_failure_reason: str
    project_id: str
    commit: CommitEvent
    repository: Repository

