from datetime import datetime

from starlette.status import (
    HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
)
from fastapi import HTTPException

from . import models


def get_projects(db, user_id, skip=0, limit=101):
    return db.query(models.Project).filter_by(client_id=user_id).offset(skip).limit(limit).all()


def get_stage(db, stage_slug, user_id):
    stage = db.query(models.Stage).filter_by(slug=stage_slug).first()
    if not stage:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Stage not found')
    project = db.query(models.Project).filter_by(id=stage.pipeline.project.id).first()
    if project and project.client.id != user_id:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='Bad User')

    return stage


def get_stage_from_name(db, pipeline_slug, stage_name, user_id):
    if stage_name is None:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Invalid Stage')

    stage = db.query(models.Stage).filter_by(name=stage_name).first()
    if not stage:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Stage Not Found')

    if stage.pipeline.slug != pipeline_slug:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Stage Not Found')

    project = db.query(models.Project).filter_by(id=stage.pipeline.project.id).first()
    if project.client.id != user_id:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='Bad User')

    return stage


def fetch_event(db, stage, job_id):
    return db.query(models.Event).filter_by(job_id=job_id, stage=stage).first()


def register_event_start(db, stage, job_unique_id):
    event = fetch_event(db, stage, job_unique_id)
    if event:
        return event

    try:
        event = models.Event(
            job_id=job_unique_id, stage=stage, stage_id=stage.id, start=datetime.utcnow()
        )
    except Exception as e:
        raise HTTPException(
            status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail='Event not created'
        )

    db.add(event)
    db.commit()
    return event


def register_event_end(db, stage, job_unique_id):
    event = fetch_event(db, stage, job_unique_id)
    if not event:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Event not found.')

    event.end = datetime.utcnow() 

    db.add(event)
    db.commit()
    return event


def register_message(db, user_id, pipeline_slug, job_id, status, message, metadata):
    user_projects = db.query(models.Project.id).filter_by(client_id=user_id)
    pipeline = db.query(models.Pipeline).filter(
        models.Pipeline.project_id.in_(user_projects),
        models.Pipeline.slug == pipeline_slug
        ).first()
    if pipeline is None:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail='Pipeline Not Found')

    try:
        message = models.PipelineMessage( 
            timestamp=datetime.utcnow(), 
            message=message or status, 
            pipeline_id=pipeline.id, 
            job_id=job_id, 
            metadata_str=metadata.json(), 
            status=status
            )
    except Exception:
        raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail='Error Creating Object')

    try:
        db.add(message)
        db.commit()
    except Exception:
        raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail='DB Error')

    return message
