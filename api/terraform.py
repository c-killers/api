from typing import Optional, List
from pydantic import BaseModel, datetime_parse, validator
from datetime import datetime
from . import crud

STAGE_NAME_MAPPING = {
    'created': 'created',
    'planning': 'planning',
    'needs_attention': 'approval',
    'applying': 'applying',
    'completed': 'completed'
}

STAGE_SEQUENCE = ('created', 'planning', 'approval', 'applying', 'completed')

IGNORE_START = ['completed', 'errored']

VERIFICATION_TRIGGER = 'verification'

class Notification(BaseModel):
    message: str
    trigger: str
    run_status: Optional[str] = ...
    run_updated_at: Optional[datetime] = ...
    run_updated_by: Optional[str] = ...

class TerraformHookEvent(BaseModel):
    payload_version: Optional[int] = ...
    notification_configuration_id: Optional[str] = ...
    run_url: Optional[str] = ...
    run_id: Optional[str] = ...
    run_message: Optional[str] = ...
    run_created_at: Optional[str] = ...
    run_created_by: Optional[str] = ...
    workspace_id: Optional[str] = ...
    workspace_name: Optional[str] = ...
    organization_name: Optional[str] = ...
    notifications: List[Notification] = []

def get_stage_name (tf_event):
    for notification in tf_event.notifications:
        for stage_str, stage_name in STAGE_NAME_MAPPING.items():
            if stage_str in notification.trigger:
                return stage_name
    return None

def is_end_required (tf_event, stage_name, db, pipeline_slug, user_id):
    if stage_name in STAGE_SEQUENCE:
        pos = STAGE_SEQUENCE.index(stage_name)
        if pos > 0:
            return True, STAGE_SEQUENCE[pos-1]
    elif stage_name == 'errored':
        for iter_stage_name in reversed(STAGE_SEQUENCE):
            iter_stage = crud.get_stage_from_name(db, pipeline_slug, iter_stage_name, user_id)
            event = crud.fetch_event(db, iter_stage, tf_event.run_id)
            if event is not None:
                return True, iter_stage_name
    return False, None


