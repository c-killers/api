from fastapi import Security, HTTPException
from fastapi.security.api_key import APIKeyQuery, APIKeyCookie, APIKeyHeader, APIKey

from . import models
from .database import SessionLocal

from starlette.status import HTTP_403_FORBIDDEN, HTTP_500_INTERNAL_SERVER_ERROR

API_KEY_NAME = 'TOKEN'

GITLAB_API_KEY_NAME = 'X-Gitlab-Token'

api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)
gitlab_api_key_header = APIKeyHeader(name=GITLAB_API_KEY_NAME, auto_error=False)
api_key_query = APIKeyQuery(name=API_KEY_NAME, auto_error=False)

async def get_user_from_key(
    api_key_header: str = Security(api_key_header),
    gitlab_api_key_header: str = Security(gitlab_api_key_header),
    api_key_query: str = Security(api_key_query),
):
    final_key = api_key_header or gitlab_api_key_header or api_key_query
    if final_key is None:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN, detail="Missing Authentication"
        )
    try:
        db = SessionLocal()
    except:
        raise HTTPException(
            status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail="DB Error"
        )

    r = db.query(models.Client).filter_by(token=final_key).first()
    if r is None:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN, detail="Invalid Token"
        )

    return r.id
