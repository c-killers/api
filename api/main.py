from typing import List

from fastapi import Depends, FastAPI

from starlette.requests import Request

from .gitlab import GitlabHookPipelineEvent, GitlabHookBuildEvent
from .terraform import TerraformHookEvent, get_stage_name, is_end_required, IGNORE_START, VERIFICATION_TRIGGER

from . import crud, schemas, misc, auth
from .database import SessionLocal

app = FastAPI()


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.get('/status')
async def get_status(db=Depends(get_db)):
    return misc.get_status(db)

@app.post('/message/gitlab/pipeline/{pipeline_slug}', response_model=schemas.PipelineMessage)
async def register_gl_pipeline_message(pipeline_slug: str, gitlab_event: GitlabHookPipelineEvent,
        user_id: auth.APIKey = Depends(auth.get_user_from_key),
        db=Depends(get_db)):
    return crud.register_message(db, user_id, pipeline_slug, 
        job_id=gitlab_event.object_attributes.id, 
        status=gitlab_event.object_attributes.status, 
        message=gitlab_event.object_attributes.detailed_status, 
        metadata=gitlab_event )

@app.post('/message/gitlab/build/{pipeline_slug}', response_model=schemas.PipelineMessage)
async def register_gl_build_message(pipeline_slug: str, gitlab_event: GitlabHookBuildEvent,
    user_id: auth.APIKey=Depends(auth.get_user_from_key),
    db=Depends(get_db)):
    return crud.register_message( db, user_id, pipeline_slug, 
        job_id=gitlab_event.commit.id, 
        status=gitlab_event.build_status, 
        message=gitlab_event.build_failure_reason if gitlab_event.build_status == 'failed' else ' '.join([gitlab_event.build_id, gitlab_event.build_name]), 
        metadata=gitlab_event )

@app.post('/message/tfe/{pipeline_slug}')#, response_model=schemas.PipelineMessage)
async def register_tfe_message(pipeline_slug: str, terraform_event: TerraformHookEvent,
        user_id: auth.APIKey = Depends(auth.get_user_from_key),
        db=Depends(get_db)):
    if VERIFICATION_TRIGGER in terraform_event.notifications[0].trigger:
        return {}
    return crud.register_message(db, user_id, pipeline_slug, 
        job_id=terraform_event.run_id, 
        status=terraform_event.notifications[0].run_status, 
        message=terraform_event.notifications[0].message, 
        metadata=terraform_event )

@app.post('/message/{pipeline_slug}', response_model=schemas.PipelineMessage)
async def register_message(pipeline_slug: str, request: misc.GenericMessageRequest,
    user_id: auth.APIKey=Depends(auth.get_user_from_key),
    db=Depends(get_db)):
    return crud.register_message( db, user_id, pipeline_slug, 
        job_id=request.id or request.buildName or request.buildId or request.buildNumber or request.job or request.jobId or request.jobNumber, 
        status=request.status or request.result or request.currentResult or request.event, 
        message=request.message or request.detail or request.description or request.text, 
        metadata=request )

@app.post('/events/tfe/{pipeline_slug}')#, response_model=List[schemas.Event])
async def tfe_event(pipeline_slug: str, terraform_event: TerraformHookEvent, db=Depends(get_db),
        user_id: auth.APIKey=Depends(auth.get_user_from_key)):
    if VERIFICATION_TRIGGER in terraform_event.notifications[0].trigger:
        return [TerraformHookEvent()]
    stage_name = get_stage_name(terraform_event) or 'errored'
    if stage_name not in IGNORE_START:
        start_stage = crud.get_stage_from_name(db, pipeline_slug, stage_name, user_id)
    end_required, end_stage_name = is_end_required(terraform_event, stage_name, db, pipeline_slug, user_id)
    generated_events = []
    if end_required:
        end_stage = crud.get_stage_from_name(db, pipeline_slug, end_stage_name, user_id)
        end_event = crud.register_event_end(db, end_stage, terraform_event.run_id)
        generated_events.append(end_event)
    if stage_name not in IGNORE_START:
        start_event = crud.register_event_start(db, start_stage, terraform_event.run_id)
        generated_events.append(start_event)
    return generated_events 


@app.get('/projects', response_model=List[schemas.Project])
async def get_projects(skip=0, limit=100, db=Depends(get_db),
        user_id: auth.APIKey=Depends(auth.get_user_from_key)):
    return crud.get_projects(db, user_id, skip=skip, limit=limit)


@app.post('/events/{stage_slug}/{job_unique_id}/start', response_model=schemas.Event)
async def start_job_event(stage_slug, job_unique_id, db=Depends(get_db),
        user_id: auth.APIKey=Depends(auth.get_user_from_key)):
    stage = crud.get_stage(db, stage_slug, user_id)
    return crud.register_event_start(db, stage, job_unique_id)


@app.post('/events/{stage_slug}/{job_unique_id}/end', response_model=schemas.Event)
async def end_job_event(stage_slug, job_unique_id, db=Depends(get_db),
        user_id: auth.APIKey=Depends(auth.get_user_from_key)):
    stage = crud.get_stage(db, stage_slug, user_id)
    return crud.register_event_end(db, stage, job_unique_id)
