from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class BaseSchema(BaseModel):
    name: str
    slug: str

    class Config:
        orm_mode = True


class Project(BaseSchema):
    pass


class Pipeline(BaseSchema):
    type: str
    project: Project


class Stage(BaseSchema):
    uri: str
    pipeline: Pipeline


class Event(BaseModel):
    job_id: str
    stage: Stage
    start: datetime
    end: Optional[datetime]

    class Config:
        orm_mode = True

class PipelineMessage(BaseModel):
    id: int
    timestamp: datetime
    message: str
    pipeline_id: int
    job_id: str
    status: str

    class Config:
        orm_mode = True
