from os import environ

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_HOST, DB_USER = environ.get('DB_HOST'), environ.get('DB_USER')
DB_NAME, DB_PASSWD = environ.get('DB_NAME'), environ.get('DB_PASSWORD')
if all((DB_HOST, DB_USER, DB_NAME, DB_PASSWD)):
    SQLALCHEMY_DATABASE_URL = f'postgresql://{DB_USER}:{DB_PASSWD}@{DB_HOST}/{DB_NAME}'
else:
    SQLALCHEMY_DATABASE_URL = environ.get('SQLALCHEMY_DATABASE_URL', 'sqlite:///../admin/ckillerdb')

if 'sqlite' in SQLALCHEMY_DATABASE_URL:
    engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False})
else:
    engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
