### BUILDER
FROM tiangolo/uvicorn-gunicorn:python3.7-alpine3.8 as builder

LABEL maintainer="C-Real Killers <c-realkillers@digitalonus.com>"

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

ADD requirements.txt /tmp/requirements.txt

RUN pip install --upgrade pip && pip install -r /tmp/requirements.txt

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r /tmp/requirements.txt

### MAIN
FROM tiangolo/uvicorn-gunicorn:python3.7-alpine3.8

RUN apk update && apk add --no-cache libpq
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /tmp/requirements.txt /tmp/requirements.txt
RUN pip install --no-cache --upgrade pip && pip install --no-cache /wheels/*

COPY ./ /app

RUN rm /app/prestart.sh

ENV SQLALCHEMY_DATABASE_URL "sqlite:////ckillerdb"

ENV MODULE_NAME api.main

ENV VARIABLE_NAME app

