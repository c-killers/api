# CKillers API

## Start the server

```
$ python3 -m venv .apienv/
$ . .apienv/bin/activate
(.apienv) $ pip install -r requirements.txt
(.apienv) $ uvicorn api.main:app --reload --port 8002
```
